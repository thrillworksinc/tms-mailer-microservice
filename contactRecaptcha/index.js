var os = require("os");
const axios = require("axios");
var Mailjet = require("node-mailjet").connect(
  `${process.env["API_KEY"]}`,
  `${process.env["API_SECRET"]}`
);

// recaptcha secret 6Ld9LKQUAAAAAE6SZSRJ7q7sZobAAkOkrmUQafmZ

module.exports = async function(context, req) {
  if (req.body.gRecaptchaResponse) {
    try {
      const gResponse = await axios({
        method: "post",
        url: "https://www.google.com/recaptcha/api/siteverify",
        params: {
          secret: process.env["GRECAPTCHA_SECRET"],
          response: req.body.gRecaptchaResponse
          //remoteip: req.userHostAddress
        }
      });
      let data = gResponse.data || {};
      if (!data.success) {
        context.log("Recaptcha check failed");
        context.res = {
          status: 400,
          body: "Invalid ReCaptcha code"
        };
      } else {
        let customerName = req.body.customerName;
        let customerEmail = req.body.customerEmail;
        let customerPhone = req.body.customerPhone;
        let customerPhoneExt = req.body.customerPhoneExt;
        let messageBody = req.body.messageBody;
        let emailPayload = {
          Messages: [
            {
              From: {
                Email: "talktous@mobileadvisors.ca",
                Name: "TMS Website"
              },
              To: [
                {
                  Email: "talktous@mobileadvisors.ca",
                  Name: "Talk to Us"
                }
              ],
              Subject: "TMS Customer Feedback",
              "Text-part":
                "Customer name: " +
                customerName +
                os.EOL +
                "Customer email: " +
                customerEmail +
                os.EOL +
                "Customer phone: " +
                customerPhone +
                os.EOL +
                "Ext: " +
                customerPhoneExt +
                os.EOL +
                "Message: " +
                messageBody,
              HTMLPart:
                "Customer name: " +
                customerName +
                "<br/>" +
                "Customer email: " +
                customerEmail +
                "<br/>" +
                "Customer phone: " +
                customerPhone +
                "<br/>" +
                "Ext: " +
                customerPhoneExt +
                "<br/>" +
                "Message: " +
                messageBody
            }
          ]
        };
        try {
          let sendEmail = await Mailjet.post("send", { version: "v3.1" });
          let response = await sendEmail.request(emailPayload);
          context.log(response.body);
          context.res = {
            body: "success"
          };
        } catch (err) {
          context.log(err.statusCode);
          context.res = {
            status: 400,
            body: "There was a problem sending the email"
          };
        }
      }
    } catch (error) {
      context.log(error);
      context.res = {
        status: 400,
        body: "There was a problem validating the reCaptcha code"
      };
    }
  }
};
