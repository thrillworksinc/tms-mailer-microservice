var os = require("os");
const axios = require("axios");


/*/
POST https://mcngdmrpzxchv6n6vd0nnf7p-lh4.auth.marketingcloudapis.com/v1/requestToken
Content-Type: application/json
 
{
    "clientId":"mrpaqnpoan90rget9jpa2nm9",
    "clientSecret":"tCWDshg0VZJjMm5fEqOHQ0EL"
}
 //*/

 /*/
 PUT https://mcngdmrpzxchv6n6vd0nnf7p-lh4.rest.marketingcloudapis.com/data/v1/async/dataextensions/key: TMS_WebSite_EmailEntries/rows
 Content-Type: application/json
 Authorization: Bearer YOUR_ACCESS_TOKEN
  
 {
    "items": [{
       "SubscriberKey":"abc@def.com", 
       "EmailAddress":"abc@def.com", 
       "FirstName":"Bobby",
       "LastName" : "Jones"
    },
    {
       "SubscriberKey":"xyz@def.com",
       "EmailAddress":"xyz@def.com",
       "FirstName":"Sam",
       "LastName" : "Sneed"
    }]
 }
//*/

module.exports = async function(context, req) {
  // if (req.body.gRecaptchaResponse) {
    try {
      const response = await axios.post('https://mcngdmrpzxchv6n6vd0nnf7p-lh4.auth.marketingcloudapis.com/v1/requestToken', {
          clientId:"vuh7x02bs8ll9axzp9xsy3jp",
          clientSecret:"vrUgYzQeVnDOsiQ5xGRHtXWs"
        }
      );
      let data = response.data || {};

      // should have an access token from SF
      if (!data.accessToken) {
        context.log("Authentication Failed");
        context.res = {
          status: 400,
          body: "Invalid Request, please try again"
        };
      } else {
        context.log('got the access token ' + data.accessToken)
        const EmailAddress = req.body.customerEmailAddress;
        const FirstName = req.body.customerFirstName;
        const LastName = req.body.customerLastName;
        context.log(FirstName + ' ' + LastName + ' ' + EmailAddress  + ' ' +  data.accessToken);
        try {
          let config = {
            headers: {
              'Authorization': 'Bearer ' + data.accessToken
            }
          }
          let submissionData = [ { 
            values: {
              FirstName ,
              LastName ,
              EmailAddress
            },
            keys: {
              SubscriberKey: EmailAddress
            }
          }];
          context.log(submissionData);

          let submitToSF = await axios.post("https://mcngdmrpzxchv6n6vd0nnf7p-lh4.rest.marketingcloudapis.com/hub/v1/dataevents/key:TMS_WebSite_EmailEntries/rowset", 
            submissionData,
            config
          )

          var submitRes = submitToSF.data || 'no response';
          context.log(submitRes);
          context.log(submitToSF);
          context.res = {
            status: 202,
            body: 'success' //{ message: "success", submitToSF: submitToSF }
          };
        } catch (err) {
          context.log(err.statusCode);
          context.res = {
            status: 400,
            body: "There was a problem submitting to SalesForce"
          };
        }
      }
    } catch (error) {
      context.log(error);
      context.res = {
        status: 400,
        body: "There was a problem getting the access code"
      };
    }
};
